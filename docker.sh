#!/bin/bash
BASEDIR="$( cd "$( dirname "$0" )" && pwd )"
docker run -it --rm\
    -v "${BASEDIR}/conf:/opt/stellar" \
    -p "8000:8000" \
    -p "11626:11626" \
    -p "11625:11625" \
    --name stellar \
    stellar/quickstart --testnet # --pubnet #