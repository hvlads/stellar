import asyncio
import logging
import os

import requests
from stellar_sdk import AiohttpClient, Server, Keypair, TransactionBuilder, Network

HORIZON_URL = "http://91.201.40.182:8000"
NETWORK = Network.TESTNET_NETWORK_PASSPHRASE  # PUBLIC_NETWORK_PASSPHRASE for real network


def get_logger(file_name):
    root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # handler_file = logging.FileHandler(os.path.join(root_dir, "logs", file_name), mode='w')
    handler_stream = logging.StreamHandler()
    format = logging.Formatter('%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')
    # handler_file.setFormatter(format)
    handler_stream.setFormatter(format)
    logger = logging.getLogger(__name__)
    # logger.addHandler(handler_file)
    logger.addHandler(handler_stream)
    logger.setLevel(logging.DEBUG)
    return logger


logger = get_logger('stellar.log')

# Activate: https://laboratory.stellar.org/#account-creator?network=test
alice_keypair = Keypair.from_secret("SDGL6NA3C2EWSQWDDBBZTL77KDR4D4THBRJYRD5TICWKN25KSN7THZ5W")

payment_address = "GDYA6DNDOY6Y4VMK45P5WZQBPQLPCOXVUYSBYD42OUBJ63TE4OJXA4WI"


async def async_payment():
    """отправка транзакции async"""
    async with Server(
            horizon_url=HORIZON_URL, client=AiohttpClient()
    ) as server:
        alice_account = await server.load_account(alice_keypair.public_key)
        base_fee = await server.fetch_base_fee()
        transaction = (
            TransactionBuilder(
                source_account=alice_account,
                network_passphrase=NETWORK,
                base_fee=base_fee,
            )
                .add_text_memo("Hello, Stellar!")
                .append_payment_op(payment_address, "10.25", "XLM")
                .build()
        )

        transaction.sign(alice_keypair)
        response = await server.submit_transaction(transaction)
        logger.debug(response)


def payment(receiver_public_key):
    server = Server(horizon_url=HORIZON_URL)
    source_account = server.load_account(alice_keypair.public_key)
    base_fee = server.fetch_base_fee()
    transaction = (
        TransactionBuilder(
            source_account=source_account,
            network_passphrase=NETWORK,
            base_fee=base_fee,
        )
            .add_text_memo("Hello, sync transaction!")  # Add a memo
            .append_payment_op(receiver_public_key, "350.1234567", "XLM")
            .set_timeout(30)
            .build()
    )
    transaction.sign(alice_keypair)
    # logger.debug(transaction.to_xdr())
    response = server.submit_transaction(transaction)
    logger.debug(response)
    return response


def create_account():
    destination = Keypair.random()
    server = Server(horizon_url=HORIZON_URL)
    source_account = server.load_account(account_id=alice_keypair.public_key)
    transaction = TransactionBuilder(
        source_account=source_account,
        network_passphrase=NETWORK,
        base_fee=100) \
        .append_create_account_op(destination=destination.public_key, starting_balance="12.25") \
        .build()
    transaction.sign(alice_keypair)
    response = server.submit_transaction(transaction)
    logger.debug("Transaction hash: {}".format(response["hash"]))
    logger.debug("New Keypair: \n\taccount id: {account_id}\n\tsecret seed: {secret_seed}".format(
        account_id=destination.public_key, secret_seed=destination.secret))


def get_balance(address):
    r = requests.get(f"{HORIZON_URL}/accounts/{address}")
    balance = None
    if r.status_code == 200:
        balance = r.json()["balances"]  # [0]["balance"]
    return balance


def get_payment_info(transaction_id):
    r = requests.get(f"{HORIZON_URL}/transactions/{transaction_id}/payments")
    return r.json()


async def payments():
    async with Server(HORIZON_URL, AiohttpClient()) as server:
        async for payment in server.payments().cursor(cursor="now").stream():
            logger.debug(f"Payment: {payment}")


async def effects():
    # https://www.stellar.org/developers/horizon/reference/resources/effect.html
    async with Server(HORIZON_URL, AiohttpClient()) as server:
        async for effect in server.effects().cursor(cursor="now").stream():
            logger.debug(f"Effect: {effect}")


async def operations():
    # https://www.stellar.org/developers/horizon/reference/resources/operation.html
    async with Server(HORIZON_URL, AiohttpClient()) as server:
        async for operation in server.operations().cursor(cursor="now").stream():
            logger.debug(f"Operation: {operation}")


async def transactions():
    # https://www.stellar.org/developers/horizon/reference/resources/transaction.html
    async with Server(HORIZON_URL, AiohttpClient()) as server:
        async for transaction in server.transactions().cursor(cursor="now").stream():
            logger.debug(f"Transaction: {transaction}")


async def listen():
    await asyncio.gather(
        payments(),
        effects(),
        operations(),
        transactions()
    )


if __name__ == '__main__':
    logger.debug(get_balance(payment_address))
    payment = payment(payment_address)
    logger.debug(payment)
    create_account()
    payment_info = get_payment_info(payment.get('id'))
    logger.debug(payment_info )
    asyncio.run(listen())
    # asyncio.run(payment())
