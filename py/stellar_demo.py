import asyncio
import logging
from abc import ABCMeta, abstractmethod
from pprint import pprint

import requests
import stellar_sdk
from stellar_sdk import Server, Keypair, TransactionBuilder, Network, AiohttpClient

NETWORK = Network.TESTNET_NETWORK_PASSPHRASE  # PUBLIC_NETWORK_PASSPHRASE for real network
# Activate: https://laboratory.stellar.org/#account-creator?network=test
ALICE_KEY_PAIR = Keypair.from_secret("SDGL6NA3C2EWSQWDDBBZTL77KDR4D4THBRJYRD5TICWKN25KSN7THZ5W")
PAYMENT_ADDRESS = "GDYA6DNDOY6Y4VMK45P5WZQBPQLPCOXVUYSBYD42OUBJ63TE4OJXA4WI"


class StellarDemoWalletABC(metaclass=ABCMeta):
    @abstractmethod
    def request(self, url, payload):
        pass

    @abstractmethod
    async def request_async(self, method, params=None):
        pass

    @abstractmethod
    def is_address(self, address) -> bool:
        return True

    @abstractmethod
    def new_address(self, amount=None):
        pass

    @abstractmethod
    def get_addresses(self):
        pass

    @abstractmethod
    def to_amount(self, amount):
        pass

    @abstractmethod
    def from_amount(self, amount):
        pass

    @abstractmethod
    def get_height(self):
        pass

    @abstractmethod
    def get_tx(self, txid):
        pass

    @abstractmethod
    def get_fee_price(self):
        pass

    @abstractmethod
    def get_balance(self, address):
        pass

    @abstractmethod
    def send(self, to, amount=None, includefee=False):
        pass

    @abstractmethod
    def transfer(self, fr, to, amount=None, includefee=False):
        pass

    @abstractmethod
    def get_version(self):
        pass

    @abstractmethod
    async def process_block(self, block):
        pass

    @abstractmethod
    async def process_tx(self, txid):
        pass

    @abstractmethod
    async def handler(self):
        pass


class StellarDemoWallet(StellarDemoWalletABC):
    def __init__(self, horizon_url):
        self.horizon_url = horizon_url
        self.server = Server(horizon_url=horizon_url)

    def request(self, url, payload=None):
        r = requests.get(url, params=payload)
        return r.json()

    async def request_async(self, method, params=None):
        """requestAsync"""
        pass

    def is_address(self, address) -> bool:
        """isAddress"""
        return True

    def new_address(self, amount=None):
        """newAddress"""
        destination = Keypair.random()
        source_account = self.server.load_account(account_id=ALICE_KEY_PAIR.public_key)
        transaction = TransactionBuilder(source_account=source_account, network_passphrase=NETWORK) \
            .append_create_account_op(destination=destination.public_key, starting_balance=amount) \
            .build()
        transaction.sign(ALICE_KEY_PAIR)
        response = self.server.submit_transaction(transaction)

        logging.getLogger('wallet.stellar').info("Transaction hash: {}".format(response["hash"]))
        logging.getLogger('wallet.stellar').info(
            "New Keypair: \n\taccount id: {account_id}\n\tsecret seed: {secret_seed}".format(
                account_id=destination.public_key, secret_seed=destination.secret))
        return response

    def get_addresses(self):
        """getAddresses"""
        pass

    def to_amount(self, amount):
        """toAmount"""
        pass

    def from_amount(self, amount):
        """fromAmount"""
        pass

    def get_height(self):
        """getHeight"""
        pass

    def get_tx(self, transaction_hash) -> dict:
        """getTx"""
        fee = self.request(url=self.horizon_url + '/transactions/' + transaction_hash)['fee_charged']
        transaction_info = self.request(url=self.horizon_url + '/transactions/' + transaction_hash + '/payments')
        info = transaction_info['_embedded']['records'][0]
        fr = info['from']
        amount = info['amount']
        to = info['to']
        tx_id = info['id']
        timestamp = info['created_at']
        asset_type = info['asset_type']
        io = [
            {'address': fr, 'dir': 'send', 'amount': amount + fee, 'account': None, 'ccy': asset_type},
            {'address': to, 'dir': 'recv', 'amount': amount, 'account': None, 'ccy': asset_type},
        ]

        return {
            'txid': tx_id,
            'io': io,
            'fee': fee,
            'time': timestamp,
        }

    def get_fee_price(self):
        """getFeePrice"""
        return self.server.fetch_base_fee()

    def get_balance(self, address) -> list:
        """getBalance"""
        balance = []

        r = requests.get(self.horizon_url + '/accounts/' + address)
        if r.status_code == 200:
            balances = r.json()
            balances = balances.get('balances', [])
            for balance_info in balances:
                asset = balance_info.get('asset_type')
                b = balance_info.get('balance')
                asset_code = 'XLM' if asset == 'native' else balance_info.get('asset_code')
                balance.append({'balance': b, 'asset_code': asset_code, 'asset_type': asset})
        else:
            print('account not found')
        return balance

    def send(self, to, amount=None, includefee=False):
        fr = ALICE_KEY_PAIR
        return self.transfer(fr, to, amount)

    def transfer(self, fr, to, amount=None, includefee=False):
        base_fee = self.server.fetch_base_fee()
        transaction = (
            TransactionBuilder(
                source_account=self.server.load_account(to),
                network_passphrase=NETWORK,
                base_fee=base_fee,
            )
                .add_text_memo("Hello, sync transaction!")  # Add a memo
                .append_payment_op(to, amount, "XLM")
                .set_timeout(30)
                .build()
        )
        transaction.sign(fr)
        response = self.server.submit_transaction(transaction)
        logging.getLogger('wallet.stellar').debug(response)
        return response

    def get_version(self) -> str:
        """getVersion"""
        return f"stellar_sdk version {stellar_sdk.__version__}"

    async def process_block(self, block):
        """processBlock"""
        pass

    async def process_tx(self, tx_id) -> None:
        """processTx"""
        # pprint(tx_id)
        logging.getLogger('wallet.stellar').info('New tx %s', tx_id)

    async def handler(self) -> None:
        async with Server(self.horizon_url, AiohttpClient()) as server:
            async for payment in server.payments().cursor(cursor="now").stream():
                asyncio.ensure_future(self.process_tx(payment))


if __name__ == '__main__':
    # Logging
    sh = logging.StreamHandler()
    logging.getLogger('wallet').addHandler(sh)
    logging.getLogger('wallet').setLevel(logging.DEBUG)
    # Init
    wallet = StellarDemoWallet(horizon_url="http://91.201.40.182:8000")
    print(wallet.new_address('500'))
    pprint(wallet.get_balance('GDY2NANYHEMA3H4EEHOCLYDWEEKPXE2QO3WIRYCLVGOIK5SNLOFUMDFP'))
    print('Balance', wallet.get_balance(PAYMENT_ADDRESS))
    print(wallet.get_version())
    print('Base fee', wallet.get_fee_price())
    pprint(wallet.send(PAYMENT_ADDRESS, "333"))
    pprint(wallet.get_tx('ce2e80902966d1237e89a60da11d7826a8fc99b0521e9bc289615580d5cfe5cc'))

    asyncio.run(wallet.handler())
